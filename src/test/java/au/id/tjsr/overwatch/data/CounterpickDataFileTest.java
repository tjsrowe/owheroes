package au.id.tjsr.overwatch.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.elemental.utils.csv.CSVParser;

import au.id.tjsr.overwatch.analysis.PairedPick;
import au.id.tjsr.overwatch.model.Hero;

public class CounterpickDataFileTest {
	@Test
	public void shouldDetectClasspathResourceLocation() {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		String resourceLocation = counters.getResourceLocation();
		Assert.assertEquals("overwatch-counters-guide-counterpicks.csv", resourceLocation);
	}
	
	@Test
	public void shouldDetectClasspathResourceType() {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		Assert.assertTrue(counters.isLocalResource());
	}
	
	@Test
	public void shouldReadCounterpicksFromClasspath() throws IOException {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		ResourceFinder finder = new ResourceFinder();
		InputStream streamForResource = finder.getStreamForResource(counters);
		InputStreamReader reader = new InputStreamReader(streamForResource);
		CSVParser parser = new CSVParser(reader);
		final String [] header = parser.getSplitLine();
		String [] line = parser.getSplitLine();
		while (line != null) {
			final String lastFullLine = parser.getLastFullLine();
			System.out.println(lastFullLine);
			line = parser.getSplitLine();
		}
		reader.close();
	}
	
	@Test
	public void shouldCalculateScoreAgainst() throws IOException {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		counters.parseFile();
		float scoreAgainst = counters.getScoreAgainst("Genji", "Mercy");
		Assert.assertEquals(1.375f, scoreAgainst, 0.001f);
		float scoreFor = counters.getScoreAgainst("Mercy", "Genji");
		Assert.assertEquals(5.0f, scoreFor, 0.05f);
		
		float winstonVsGenji = counters.getScoreAgainst("Winston", "Genji");
		Assert.assertEquals(1.0f, winstonVsGenji, 0.005f);
		float genjiVsWinston = counters.getScoreAgainst("Genji", "Winston");
		Assert.assertEquals(4.88f, genjiVsWinston, 0.005f);
		
		float mccreeCounteringGenji = counters.getScoreAgainst("Genji", "McCree");
	}
	
	@Test
	public void shouldCompareLoadedData() throws IOException {
		CounterResource countersResource = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		countersResource.parseFile();
		List<PairedPick> counters = countersResource.getCountersTo("Genji");
		
//		float scoreAgainst = counters.getScoreAgainst("Genji", "Mercy");
//		Assert.assertEquals(4.625f, scoreAgainst, 0.001f);
//		float scoreFor = counters.getScoreAgainst("Mercy", "Genji");
//		Assert.assertEquals(1.0f, scoreFor, 0.05f);
//		
//		float winstonVsGenji = counters.getScoreAgainst("Winston", "Genji");
//		Assert.assertEquals(5.0f, winstonVsGenji, 0.005f);
//		float genjiVsWinston = counters.getScoreAgainst("Genji", "Winston");
//		Assert.assertEquals(1.117f, genjiVsWinston, 0.005f);
		
	}
	
	@Test
	public void shouldParseScores() {
		String origLine = "counterpicks,Genji,Bastion,5,16,0,2,0,1";
		String [] line = origLine.split(",");
		int [] scores = CounterResource.getScoreList(line);
		int [] expected = {0,16,0,2,0,1};
		Assert.assertTrue("Output array did not match.", Arrays.equals(expected, scores));
	}
	
	@Test
	public void shouldCreateCounter() {
		CounterResource counters = new CounterResource();
		String heroToBeCountered = "Bastion";
		String heroToCounterWith = "Genji";
		int[] scores = {0,16,0,2,0,1};
		counters.findOrCreateCounterSet(heroToBeCountered, heroToCounterWith, scores);
		float medianCounterScore = counters.getScoreAgainst("Bastion", "Genji");
		Assert.assertEquals(4.57f, medianCounterScore, 0.01f);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void shouldParseRow() {
		CounterResource counters = new CounterResource();
		String origLine = "counterpicks,Genji,Bastion,5,16,0,2,0,1";
		String [] line = origLine.split(",");
		counters.parseLine(line);
		float medianCounterScore = counters.getScoreAgainst("Bastion", "Genji");
		List<PairedPick> countersToGenji = counters.getCountersTo("Bastion");
		float medianScore = countersToGenji.get(0).getMedianScore();
		Assert.assertEquals(4.57f, medianCounterScore, 0.01f);
		Assert.assertEquals(4.57f, medianScore, 0.01f);
	}
	
	@Test
	public void shouldReadCounterFile() throws IOException {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		counters.parseFile();
	}

	@Test
	public void shouldGetMedianScore() throws IOException {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		counters.parseFile();
		List<PairedPick> picks = counters.getCountersTo("Genji");
		Assert.assertEquals(21, picks.size());
	}
	
	@Test
	public void shouldGetCountersInOrder() throws IOException {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		counters.parseFile();

		List<PairedPick> orderedCounters = counters.getOrderedCounters("Genji");
		final Hero first = orderedCounters.get(20).getHero();
		final Hero second = orderedCounters.get(19).getHero();
		final Hero third = orderedCounters.get(18).getHero();
		
		Assert.assertEquals("Winston", first.getHeroName());
		Assert.assertEquals("Mei", second.getHeroName());
		Assert.assertEquals("Roadhog", third.getHeroName());
	}
	
	@Test
	public void shouldSortCorrectly() throws IOException {
		CounterResource counters = new CounterResource("classpath:overwatch-counters-guide-counterpicks.csv");
		counters.parseFile();

		List<PairedPick> countersToGenji = counters.getCountersTo("Genji");
		Collections.sort(countersToGenji);
	}
}
