package au.id.tjsr.overwatch.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.id.tjsr.overwatch.analysis.PairedPick;

public class HeroUnitTest {
	private Hero alpha;
	private Hero beta;
	private Hero charlie;

	@Before
	public void setUp() {
		alpha = new Hero("Alpha");
		beta = new Hero("Beta");
		alpha.findOrCreateCounter(beta);
		charlie = new Hero("Charlie");
		charlie.findOrCreateSynergy(alpha);
	}
	@Test
	public void shouldAddCounterHero() {
		Assert.assertTrue(alpha.hasCounter(beta));
	}
	
	@Test
	public void shouldFindCounterHero() {
		PairedPick counter = alpha.findCounter(beta);
		Assert.assertNotNull(counter);
	}
}
