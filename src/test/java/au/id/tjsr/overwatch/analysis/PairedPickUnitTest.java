package au.id.tjsr.overwatch.analysis;

import org.junit.Assert;
import org.junit.Test;

import au.id.tjsr.overwatch.model.Hero;

public class PairedPickUnitTest {
	@Test
	public void shouldGetMedian() {
		Hero mercy = new Hero("Mercy");
		PairedPick pick = new PairedPick(mercy);
		pick.setScoreCount(1, 1);
		pick.setScoreCount(2, 2);
		pick.setScoreCount(3, 3);
		pick.setScoreCount(4, 2);
		pick.setScoreCount(5, 1);
		float medianScore = pick.getMedianScore();
		Assert.assertEquals(3.0f, medianScore, 0.05f);
	}
	
	@Test
	public void shouldComparePairs() {
		Hero mercy = new Hero("Mercy");
		PairedPick pickMercy = new PairedPick(mercy);
		pickMercy.setScoreCount(1, 1);
		pickMercy.setScoreCount(2, 2);
		pickMercy.setScoreCount(3, 3);
		pickMercy.setScoreCount(4, 2);
		pickMercy.setScoreCount(5, 1);

		Hero hanzo = new Hero("Hanzo");
		PairedPick pickHanzo = new PairedPick(hanzo);
		pickHanzo.setScoreCount(1, 1);
		pickHanzo.setScoreCount(2, 1);
		pickHanzo.setScoreCount(3, 1);
		pickHanzo.setScoreCount(4, 7);
		pickHanzo.setScoreCount(5, 7);

		int comparison1 = pickMercy.compareTo(pickHanzo);
		Assert.assertEquals(-1, comparison1);
		
		int comparison2 = pickHanzo.compareTo(pickMercy);
		Assert.assertEquals(1, comparison2);
	}
}
