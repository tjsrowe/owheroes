package au.id.tjsr.overwatch.model;

public class Arena {
	private String name;
	
	public Arena(final String arenaName) {
		setName(arenaName);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
