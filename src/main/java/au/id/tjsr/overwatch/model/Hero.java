package au.id.tjsr.overwatch.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.id.tjsr.overwatch.analysis.PairedPick;

public class Hero {
	private String heroName;
	private Map<Hero, PairedPick> counterPicks;
	private Map<Hero, PairedPick> synergyPicks;
	
	public Hero(final String name) {
		this.setHeroName(name);
		this.counterPicks = new HashMap<Hero, PairedPick>();
		this.synergyPicks = new HashMap<Hero, PairedPick>();
	}

	public String getHeroName() {
		return heroName;
	}

	public void setHeroName(String heroName) {
		this.heroName = heroName;
	}

	PairedPick findOrLinkTo(Hero hero, Map<Hero, PairedPick> list) {
		PairedPick pick = list.get(hero);
		if (pick == null) {
			pick = new PairedPick(hero);
			list.put(hero, pick);
		}
		return pick;
	}

	public PairedPick findOrCreateCounter(Hero counterHero) {
		return findOrLinkTo(counterHero, this.counterPicks);
	}
	
	public PairedPick findOrCreateSynergy(Hero counterHero) {
		return findOrLinkTo(counterHero, this.synergyPicks);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((heroName == null) ? 0 : heroName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hero other = (Hero) obj;
		if (heroName == null) {
			if (other.heroName != null)
				return false;
		} else if (!heroName.equals(other.heroName))
			return false;
		return true;
	}

	public PairedPick findCounter(Hero hero) {
		PairedPick pick = this.counterPicks.get(hero);
		return pick;
	}

	public boolean hasCounter(Hero beta) {
		return this.counterPicks.containsKey(beta);
	}
	
	public String toString() {
		return this.getHeroName();
	}

	public List<PairedPick> listCountersInOrder() {
		List<PairedPick> counterPickValues = new ArrayList<PairedPick>(counterPicks.values());
		Collections.sort(counterPickValues);
		return counterPickValues;
	}

	public List<PairedPick> getCounters() {
		List<PairedPick> counterPickValues = new ArrayList<PairedPick>(counterPicks.values());
		return counterPickValues;
	}
}
