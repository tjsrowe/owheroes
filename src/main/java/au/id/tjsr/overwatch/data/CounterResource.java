package au.id.tjsr.overwatch.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.id.tjsr.overwatch.analysis.PairedPick;
import au.id.tjsr.overwatch.model.Hero;

public class CounterResource extends AbstractResource implements OverwatchDataResource {
	static final int MAX_SCORE = 5;
	public CounterResource(String location) {
		super();
		super.getLocationForString(location);
	}
	
	CounterResource() {
		super();
	}
	
	public void parseFile() throws IOException {
		ResourceFinder finder = new ResourceFinder();
		InputStream streamForResource = finder.getStreamForResource(this);
		getParser(streamForResource);
		
		String [] header = getHeader();
		final String headerFullLine = getLastFullLine();
		System.out.println(headerFullLine);
		String[] line = getNextLine();
		while (line != null) {
			final String lastFullLine = getLastFullLine();
			try {
				parseLine(line);
				System.out.println(lastFullLine);
			} catch (NumberFormatException ex) {
				System.err.println(lastFullLine);
			}
			line = getNextLine();
		}
		streamForResource.close();
	}

	void parseLine(String[] line) {
		final String heroToCounterWithName = line[1];
		final String heroToBeCounteredName = line[2];
		int [] scores = getScoreList(line);
		findOrCreateCounterSet(heroToBeCounteredName, heroToCounterWithName, scores);
	}

	void findOrCreateCounterSet(String heroToBeCounteredName, String heroToCounterWithName, int[] scores) {
		Hero heroToBeCountered = findOrCreateAgent(heroToBeCounteredName);
		PairedPick pick = findOrCreateCounter(heroToBeCountered, heroToCounterWithName);
		if (pick == null) {
			return;
		}
		setScores(pick, scores);
	}

	private void setScores(PairedPick pick, int[] scores) {
		int scoreValue = 5;
		int scoreIndex = 1;
		while (scoreIndex <= 5) {
			int count = scores[scoreIndex];
			pick.setScoreCount(scoreValue, count);
			scoreValue--;
			scoreIndex++;
		}
	}

	static int[] getScoreList(String[] line) {
		int [] scores = new int[MAX_SCORE + 1];
		// Scores are in reverse order
		int currentScore = MAX_SCORE;
		for (int columnNumber = 8;columnNumber >= 4;columnNumber--) {
			final String stringScoreValue = line[columnNumber];
			final int score = Integer.parseInt(stringScoreValue);
			scores[currentScore] = score;
			currentScore--;
		}
		return scores;
	}

	private PairedPick findOrCreateCounter(Hero heroToBeCountered, String counterHeroName) {
		final String heroToBeCounteredName = heroToBeCountered.getHeroName();
		if (heroToBeCounteredName.equals(counterHeroName)) {
			// Skip if it's the same hero.
			return null;
		}
		Hero counterHero = findOrCreateAgent(counterHeroName);
		PairedPick counter = heroToBeCountered.findOrCreateCounter(counterHero);
		return counter;
	}

	public float getScoreAgainst(String heroToBeCounteredName, String heroToCounterWithName) {
		Hero heroToBeCountered = findAgent(heroToBeCounteredName);
		Hero heroToCounterWith = findAgent(heroToCounterWithName);
		PairedPick counter = heroToBeCountered.findCounter(heroToCounterWith);
		if (counter != null) {
			float medianScore = counter.getMedianScore();
			return medianScore;
		}
		return 0.0f;
	}

	public List<PairedPick> getOrderedCounters(String agent) {
		Hero hero = findAgent(agent);
		List<PairedPick> counters = hero.listCountersInOrder();
		return counters;
	}

	public List<PairedPick> getCountersTo(String agent) {
		Hero hero = findAgent(agent);
		List<PairedPick> counters = hero.getCounters();
		return counters;
	}
	
	public List<PairedPick> getBestPicksAgainst(final String ...opposingHeroes) {
		Map<Hero, Float> heroScores = new HashMap<Hero, Float>();
		for (final String hero : opposingHeroes) {
			List<PairedPick> countersToCurrentHero = getCountersTo(hero);
			for (PairedPick pick : countersToCurrentHero) {
				Hero counterHero = pick.getHero();
				float counterScore = pick.getMedianScore();
				if (heroScores.containsKey(counterHero)) {
					float cumulativeCounterScore = heroScores.get(counterHero) + counterScore;
					heroScores.remove(counterHero);
					heroScores.put(counterHero, cumulativeCounterScore);
				} else {
					heroScores.put(counterHero, counterScore);
				}
			}
		}
	}
}
