package au.id.tjsr.overwatch.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class ResourceFinder {
	private static final String RESOURCE_PACKAGE = "au/id/tjsr/overwatch/data";
	private String synergyFileLocation;
	private String counterFileLocation;
	private String arenaFileLocation;
	
	public InputStream getUrlResource(OverwatchDataResource resource) throws IOException {
		URL urlResouce = new URL(resource.getResourceLocation());
		return urlResouce.openStream();
	}
	
	public InputStream getResource(OverwatchDataResource resource) {
		ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
		final String location = RESOURCE_PACKAGE + "/" + resource.getResourceLocation();
		return systemClassLoader.getResourceAsStream(location);
	}
	
	public InputStream getFileResource(OverwatchDataResource resource) throws FileNotFoundException {
		File inputFile = new File(resource.getResourceLocation());
		FileInputStream fis = new FileInputStream(inputFile);
		return fis;
	}
	
	public InputStream getStreamForResource(OverwatchDataResource resource) throws IOException {
		InputStream stream = null;
		if (resource.isFileResource()) {
			stream = getFileResource(resource);
		} else if (resource.isLocalResource()) {
			stream = getResource(resource);
		} else if (resource.isUrlResource()) {
			stream = getUrlResource(resource);
		}
		return stream;
	}

	public String getSynergyFileLocation() {
		return synergyFileLocation;
	}

	public void setSynergyFileLocation(String synergyFileLocation) {
		this.synergyFileLocation = synergyFileLocation;
	}

	public String getCounterFileLocation() {
		return counterFileLocation;
	}

	public void setCounterFileLocation(String counterFileLocation) {
		this.counterFileLocation = counterFileLocation;
	}

	public String getArenaFileLocation() {
		return arenaFileLocation;
	}

	public void setArenaFileLocation(String arenaFileLocation) {
		this.arenaFileLocation = arenaFileLocation;
	}
}
