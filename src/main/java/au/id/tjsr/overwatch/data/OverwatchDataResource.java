package au.id.tjsr.overwatch.data;

public interface OverwatchDataResource {
	public String getResourceLocation();
	public boolean isLocalResource();
	public boolean isFileResource();
	public boolean isUrlResource();
}
