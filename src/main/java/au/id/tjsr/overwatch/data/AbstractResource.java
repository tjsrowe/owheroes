package au.id.tjsr.overwatch.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.elemental.utils.csv.CSVParser;

import au.id.tjsr.overwatch.model.Hero;

public abstract class AbstractResource {
	private String resourceLocation;
	private boolean claspathResource = false;
	private CSVParser parser;
	private List<Hero> allHeroes;
	
	protected AbstractResource() {
		this.allHeroes = new ArrayList<Hero>();
	}
	
	public void getLocationForString(final String location) {
		String[] parts = location.split(":", 2);
		if ("classpath".equals(parts[0])) {
			this.claspathResource = true;
			resourceLocation = parts[1];
		}
	}
	
	public String getResourceLocation() {
		return this.resourceLocation;
	}
	
	public boolean isLocalResource() {
		return this.claspathResource;
	}
	
	public boolean isFileResource() {
		//TODO: Parse file resource paths
		return false;
	}
	
	public boolean isUrlResource() {
		//TODO: Parse URL resource paths
		return false;
	}
	
	protected String[] getNextLine() throws IOException {
		String [] line = parser.getSplitLine();
		return line;
	}
	
	protected String getLastFullLine() {
		return parser.getLastFullLine();
	}

	protected String[] getHeader() throws IOException {
		final String [] header = parser.getSplitLine();
		return header;
	}

	protected CSVParser getParser(InputStream streamForResource) throws IOException {
		InputStreamReader reader = new InputStreamReader(streamForResource);
		parser = new CSVParser(reader);
		return parser;
	}

	protected Hero findAgent(String agentName) {
		for (Hero current : this.allHeroes) {
			if (agentName.contentEquals(current.getHeroName())) {
				return current;
			}
		}
		return null;
	}

	public void setHeroList(List<Hero> heroes) {
		this.allHeroes = heroes;
	}
	
	protected Hero findOrCreateAgent(String heroName) {
		Hero currentHero = findAgent(heroName);
		if (currentHero == null) {
			currentHero = createHero(heroName);
		}
		return currentHero;
	}

	protected Hero createHero(String name) {
		Hero hero = new Hero(name);
		this.allHeroes.add(hero);
		return hero;
	}
}
