package au.id.tjsr.overwatch.analysis;

import java.util.HashMap;
import java.util.Map;

import au.id.tjsr.overwatch.model.Hero;

public class PairedPick implements Comparable<PairedPick> {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hero == null) ? 0 : hero.hashCode());
		result = prime * result + ((pickScoreCount == null) ? 0 : pickScoreCount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PairedPick other = (PairedPick) obj;
		if (hero == null) {
			if (other.hero != null)
				return false;
		} else if (!hero.equals(other.hero))
			return false;
		if (pickScoreCount == null) {
			if (other.pickScoreCount != null)
				return false;
		} else if (!pickScoreCount.equals(other.pickScoreCount))
			return false;
		return true;
	}

	private Hero hero;
	private Map<Integer, Integer> pickScoreCount;
	
	public PairedPick(Hero hero) {
		this.pickScoreCount = new HashMap<Integer, Integer>();
		this.hero = hero;
	}
	
	public void setScoreCount(int score, int count) {
		pickScoreCount.put(score, count);
	}
	
	public float getMedianScore() {
		int total = 0;
		int count = 0;
		for (int score = 5;score >= 1;score--) {
			int scoreCount = this.pickScoreCount.get(score);
			total = total + (scoreCount * score);
			count = count + scoreCount;
		}
		float median = total / (float)count;
		return median;
	}
	
	public int getModeScore() {
		int maxOccursCount = 0;
		int maxOccursScore = 0;
		for (int score = 5;score >= 1;score--) {
			int scoreCount = this.pickScoreCount.get(score);
			if (scoreCount > maxOccursCount){
				maxOccursCount = scoreCount;
				maxOccursScore = score;
			}
		}
		return maxOccursScore;
	}

	@Override
	public int compareTo(PairedPick other) {
		if (other == null) {
			return 1;
		}
		float medianThis = this.getMedianScore();
		float medianOther = other.getMedianScore();
		float compare = medianOther - medianThis;
		if (compare > 0.0f) {
			return -1;
		} else if (compare < 0.0f) {
			return 1;
		} else {
			return 0;
		}
	}
	
	public String toString() {
		final String heroName = this.hero.getHeroName();
		final float medianScore = this.getMedianScore();
		final String line = String.format(":%s=%1.2f", heroName, medianScore);
		return line;
	}

	public Hero getHero() {
		return this.hero;
	}
}
